// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCJMGZOZaZxaEdUeeGOLSmTquZ1RGl2EGE",
  authDomain: "convocaproto.firebaseapp.com",
  projectId: "convocaproto",
  storageBucket: "convocaproto.appspot.com",
  messagingSenderId: "2256743530",
  appId: "1:2256743530:web:fcc1298a9538b5b50a2e13"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
