 import React from 'react'
 import {Routes,Route} from "react-router-dom"
import {Home} from './components/Home'
import {Login} from './components/Login'
import {Register} from './components/Register'
import {AuthProvider} from './context/authContext'
import {ProtectedRoute} from "./components/ProtectedRoute"
import './App.css';
import { LandingPage } from './components/LandingPage'
import { Denuncia } from './components/Denuncia'
import { Listado } from './components/Listado'
 function App() {
   return (
    <div className='  h-screen '>
      <AuthProvider>
      <Routes >
       <Route path="/bienvenido" element={<ProtectedRoute><Home></Home></ProtectedRoute> }></Route>
       <Route path="/register" element={<Register></Register>} ></Route>
       <Route path="/login" element={<Login></Login>} ></Route>
       <Route path="/" element={<LandingPage></LandingPage>} ></Route>

       <Route path="/listado" element={<ProtectedRoute><Listado></Listado></ProtectedRoute> } ></Route>
       <Route path="/denuncia" element={<ProtectedRoute><Denuncia ></Denuncia></ProtectedRoute> } ></Route>
     </Routes>
      </AuthProvider>
    </div>
   )
 }
 
 export default App