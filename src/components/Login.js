import React,{useState} from 'react'
import { Link, useNavigate } from 'react-router-dom';
import {useAuth} from '../context/authContext';
import { Alert } from './Alert';
import '../App.css';
import logo from '../assets/logo.png'; 
export function Login() {
  const [user,setUser]=useState({
      email:'',
      password:'',
  })

const {login,logingoogle}= useAuth()
const navigate = useNavigate()
const [error,setError]=useState();

  const handleChange = ({target:{name,value}}) =>{
     
      setUser({...user,[name]: value})
  }

  const handleSubmit=async(e)  =>{
      e.preventDefault();
      setError( '')
      try {
        await login(user.email,user.password) ;
        navigate('/denuncia');  }
      catch(error){
       console.log(error);
      setError(error.message)
      }   };



const handleGoogleLogin=async ()=>{
  try {
    await logingoogle();
    navigate("/denuncia")
} catch (error) {
  setError(error.message)
}}
return (
<div className='w-full h-screen flex bg-img--container'>
  <div className='w-full max-w-xs m-auto  '>
    
{error && <Alert message={error}/>}
      <form onSubmit={handleSubmit} className="bg-white shadow-md rouded px-8 pt-6 pb-8 mb-4">
        <div className="flex w-100 justify-center ">
    <img className='w-40' src={logo} alt="Logo" />
    </div>
<div className="mb-4">
  <label htmlFor="email" className='block text-gray-700 text-sm font-bold mb-2'>Email</label>
  <input type="email" name="email" placeholder="" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
  focus:shadow-outline' onChange={handleChange}/>
</div>

<div className="mb-4" >
  <label htmlFor="password" className='block text-gray-700 text-sm font-bold mb-2'>Password</label>
  <input type="password" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
  focus:shadow-outline text-sm' name="password" id="" onChange={handleChange} />
</div>
<button className='bg-red-500 hover:bg-red-700  text-white font-bold py-2 px-4 border rounded focus:outline-none focus:shadow-outline'> Ingresa</button>

  </form>
  <p className='my-4 text-sm flex justify-between px-4'> No tienes una cuenra? <Link to="/register">registroo</Link> </p>
  <button className='bg-red-500 hover:bg-red-700  text-white  shadow-md border rounded border-2 border-gray-00 py-2 px-4 w-full' onClick={handleGoogleLogin}>Google</button>
 </div>
</div>

 
)
}
