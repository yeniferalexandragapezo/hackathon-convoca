import { useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/logo.png'; 
import '../App.css';

export function Denuncia( )
{

   const  handleSubmit =()=>{

   }
   const [user,setUser]=useState({
      email:'',
      password:'',
  })

  const [pages,setPage]=useState("primer");
 

  const handleChange = ({target:{name,value}}) =>{
     
      setUser({...user,[name]: value})
  }

    return  (<>
  <header className="border-b">
  <div className="container">
    <div className="flex items-center justify-between">
      <div className="logo">
        <img className="h-20 p-2" src={logo}  alt="logo"/>
      </div>
      <nav className="navbar flex items-center gap-8">
      <p   className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900"> <Link to="/"> Home</Link> </p>
        <p className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900">  <Link to="/ "> Recursos</Link> </p>
        <p  className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900">  <Link to="/listado">Denuncias</Link> </p>
 
      </nav>
    </div>
  </div>
</header>
{pages === "primer" &&
    <section   className="d-none py-12">
  
  <div className='w-full w-70 m-auto  '>
      <form onSubmit={handleSubmit} className="bg-white shadow-md rouded px-8 pt-6 pb-8 mb-4">
          <h1 className="anim-text text-4xl">Hola, somos <b>ALZA TU VOZ</b> un <b> aliado</b>  frente a <b>la violencia</b></h1>
           
    <div className="mb-4">
      <label htmlFor="titulo" className='block text-gray-700 text-sm font-bold mb-2'>titulo de la denuncia</label>
      <input type="text" name="titulo" placeholder="" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
      focus:shadow-outline' onChange={handleChange}/>
    </div>
    <div className="w-full flex justify-end">
    <button className='bg-red-500 hover:bg-red-700  text-white font-bold py-2 px-4 border rounded focus:outline-none focus:shadow-outline' onClick={()=>setPage("segundo")}> Continuar</button>
    </div>
    <div className="bg-gray-200 border-gray-400 text-black-700 my-4 px-4 py-2 rounded relative mb-2  ">
     <span className="sm:inline block font-bold ">Recomendaciones</span>
     <p className=" ">Se breve</p>
     <p className=" ">Expresa la urgencia</p>
     <p className=" ">Expresa la problematica</p>
    </div>
      </form>
      
     </div>
  
</section>}
 


 
{pages === "segundo" &&
<section className="py-12">
  
  <div className='w-full  w-70 m-auto  '>
    
 
          <form onSubmit={handleSubmit} className="bg-white shadow-md rouded px-8 pt-6 pb-8 mb-4">
          <h1 className="anim-text text-4xl"> <b>Agrega una descripcion </b>  </h1>
          <p className='my-2 block text-gray-700 text-smmb-2 font-bold '>Explica el problema y porue te preocupa:Muestra como eso te impacta a ti,tus familiares o comunidad  </p>
            
    <div className="mb-4">
      <label htmlFor="desc" className=''>Explicanos el problema</label>
      <textarea type="text" name="desc" placeholder="" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
      focus:shadow-outline' onChange={handleChange}/>
    </div>
  
    <div className="  my-4 px-4 py-2 relative mb-2  ">
 
    <p className="my-2">Entendemos tu preocupación, cómo clasificarías tu denuncia para darle un mejor tratamiento</p>
 
 <div className="flex   flex-wrap">
 <span className="bg-stone-400  m-1  hover:bg-stone-700  text-white uppercase py-1 px-3.5 rounded-full text-sm">Lenguaje de Odio</span>
 <span className="bg-stone-400  m-1  hover:bg-stone-700  text-white uppercase py-1 px-3.5 rounded-full text-sm">Contenido Violento</span>
 <span className="bg-stone-400  m-1  hover:bg-stone-700  text-white uppercase py-1 px-3.5 rounded-full text-sm">Derechos humanos</span>
 <span className="bg-stone-400 m-1  hover:bg-stone-700 text-white uppercase py-1 px-3.5 rounded-full text-sm">Lenguaje Sexista</span>
 <span className="bg-stone-400  m-1  hover:bg-stone-700  text-white uppercase py-1 px-3.5 rounded-full text-sm">Lenguaje de Odio</span>
 <span className="bg-stone-400  m-1  hover:bg-stone-700   text-white uppercase py-1 px-3.5 rounded-full text-sm">Contenido Violento</span>
 <span className="bg-stone-400  m-1  hover:bg-stone-700  text-white uppercase py-1 px-3.5 rounded-full text-sm"> Derechos humanos</span>

 </div>
<div className="w-full flex justify-end">
<div className="flex justify-between my-5 w-40">
   <button className='bg-slate-500 hover:bg-slate-700  text-white font-bold py-2 px-4 border rounded focus:outline-none focus:shadow-outline' onClick={()=>setPage("primer")}> Atras</button>
   <button className='bg-red-500 hover:bg-red-700  text-white font-bold py-2 px-4 border rounded focus:outline-none focus:shadow-outline' onClick={()=>setPage("tercer")}> Continuar</button>
   </div>
</div>
    
    </div>
      </form>
      
     </div>
  
</section> }

{pages === "tercer" &&
<section className="py-12">
  
  <div className='w-full  w-70 m-auto  '>
    
 
          <form onSubmit={handleSubmit} className="bg-white shadow-md rouded px-8 pt-6 pb-8 mb-4">
          <h1 className="anim-text text-4xl"> <b>Especifica el medio</b>  </h1>
          <p className='my-2 block text-gray-700 text-smmb-2 font-bold '>Cuentanos ue medio de comunicacion incurrio la falta   </p>
            
    <div className="mb-4">
      <label htmlFor="desc" className=''>Especifica la plataforma</label>
      <div class="flex justify-center">
      <div class="mb-3 xl:w-96">
        <select class="form-select appearance-none
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding bg-no-repeat
          border border-solid border-gray-300
          rounded
          transition
          ease-in-out
          m-0
          focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"  >
            <option selected>Tipo de medio</option>
            <option value="1">Audiovisual</option>
            <option value="2">Radio</option>
            <option value="3">Escrito</option>
        </select>
      </div>
</div>
    </div>

    <div className="mb-4">
      <label htmlFor="desc" className=''>Entidad ue incurrio</label>
      <div class="flex justify-center">
      <div class="mb-3 xl:w-96">
        <select class="form-select appearance-none
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding bg-no-repeat
          border border-solid border-gray-300
          rounded
          transition
          ease-in-out
          m-0
          focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"  >
            <option selected>Medio en cuestion</option>
            <option value="1">El Comercio</option>
            <option value="2">Expresso</option>
            <option value="3">Peru21</option>
            <option value="3">Trome</option>
            <option value="5">Otro</option>
        </select>
      </div>
</div>
    </div>
    <div className="mb-4">
    <div class="flex items-center justify-center w-full mb-12">
  
  <label for="toggleB" class="flex items-center cursor-pointer">
 
    <div class="relative">
      
      <input type="checkbox" id="toggleB" class="sr-only"/>
       
      <div class="block bg-gray-600 w-14 h-8 rounded-full"></div>
      
      <div class="dot absolute left-1 top-1 bg-white w-6 h-6 rounded-full transition"></div>
    </div>
    
    <div class="ml-3 text-gray-700 font-medium">
      Deseas ue la denuncia sea anonima
    </div>
  </label>

</div>
    </div>
  
    <div className="  my-4 px-4 py-2 relative mb-2  ">
 
     
 
  <div className="w-full flex justify-end">
<div className="flex justify-between my-5 w-40">
   <button className='bg-slate-500 hover:bg-slate-700  text-white font-bold py-2 px-4 border rounded focus:outline-none focus:shadow-outline' onClick={()=>setPage("segundo")}> Atras</button>
  <Link to="/listado"> <button className='bg-red-500 hover:bg-red-700  text-white font-bold py-2 px-4 border rounded focus:outline-none focus:shadow-outline'  > Finalizar</button></Link>
   </div>
</div>

    
    </div>
      </form>
      
     </div>
  
</section> }
    </>)
}