import { Link } from 'react-router-dom';
import logo from '../assets/logo.png'; 
import mujer from "../assets/pexels-andrea-piacquadio-3761509.jpg"
import mujer2 from "../assets/pexels-mart-production-7699507.jpg"
export function LandingPage( ){
    return  (<>
    <header className="border-b">
  <div className="container">
    <div className="flex items-center justify-between">
      <div className="logo">
        <img className="h-20 p-2" src={logo}  alt="logo"/>
      </div>
      <nav className="navbar flex items-center gap-8">
      <p   className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900"> <Link to="/"> Home</Link> </p>
        <p className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900">  <Link to=" "> Recursos</Link> </p>
        <p  className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900">  <Link to=" "> </Link> </p>

        <button className="flex mx-auto   text-white bg-red-500 border-0 py-2 px-8 focus:outline-none hover:bg-red-600 rounded text-lg"  ><Link to="/login">Ingresa</Link> </button>
  
      </nav>
    </div>
  </div>
</header>

<section id="hero" className="py-12">
  <div className="container">
    <div className="grid grid-cols-2 items-center gap-8">
      <div className="hero-info flex flex-col gap-4">
        <h1 className="anim-text text-4xl"><b>Notifica</b> las <b> irregularidades</b> <b>en </b> <b>los medios </b></h1>
        <p className="anim-text font-light text-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum, vel modi quos omnis id doloremque optio quia debitis magni animi.</p>
        <div className="hero-action flex justify-center">
          <button className="py-2 px-4 text-white bg-red-500 border-0 py-2 px-8 focus:outline-none hover:bg-red-600 rounded text-lg focus:ring focus:ring-red-300 transition-all">
          <Link to="/register">Denuncia</Link>
            </button>
          <button className="py-2 px-4 bg-white text-black border border-red-500 hover:bg-red-600 hover:text-white focus:bg-red-500 focus:text-white focus:ring focus:ring-red-300 transition-all">  <Link to="/register">Informate</Link></button>
        </div>
      </div>
      <div className="hero-img">
        <img className="w-full h-96 object-cover object-top shadow-md" src={mujer}   alt="sample" />
      </div>
    </div>
  </div>
</section>

<section>
    <div className="container gap-8   m-auto ">
    <div className="flex justify-center  m-auto ">
  <div className="flex-1 ">
    <img className="p-4 w-10  " src="https://convoca.pe/latamchequea-escudo/assets/img/logos/convoca-web-6-anios.png" alt="" />
  </div>
  <div className="flex-1 ">
    <img className="p-4 w-10  "  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6e6hF69H0Yle0RvMp_O-JZkMf8GvvRgbY236anm0toeZpHdBOLX2ASK7VbbsGcxqxESw&usqp=CAU" alt="" />
  </div>
  <div className="flex-1 ">
   <img className="p-4 w-10  "  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAC01BMVEVXV1f/CBz///9HR0f/CBj/AADt7e1gVVX/CCH/DRpYWFjwdXxVVVUAAAD/AyT8/PxQUFDk5ORjY2PJyclwcHBERES6urr5AACfn5/5////ACinp6f/AAz39/fzIzLqqKf6fn7/cXb/8vCampr6v735ra7/+P/c3Ny1tbXz///9JzLQ0ND8497/AC71PEOBgYH3w8g7OzvtAB3FzVvMyFr1lJv41NFqamp1dXWGhoYfHx8QEBAAeUP+//gATCb/+vSzAAAAjUymAAAAXioAb0zKAAjRACHMAACzrp3/8///y1qLAAC0ABgjAAAqKiqzrbUmZkMATRwAPyIaKB4Ab0KltKoAfj0AlD8AZiGCm47j+/RicmkAWTEbTC4VQSwbNyMBhzo0j2rq4s7u7cX6/+cAOhYAIhIAhU3i3Jjc3jUAKgAAEADX5ark1lnZ1rXV3VCgoVQYUjqptmHIznKzfXVnUUeod2H3+NS/u3/gM0+lABx5ByhLdF99SE3IITZ6hHyFABONgGbgz9fXhY7phoZ0ExoAeSp0RkeVZ2iIrpJlknZuAAC2lZuqKTLgAADsITgAmC5EKCm7pmGsl4FAbz+QhFXHY2zWuLrAt6Opwlvv5XfKvGipu6PMn2cymaaCkl5niaPcxl/lZH69fEFKxeVDmsFbimZajodIxvjaqVUAyuNlvuJxQx/M6dUIw/ZxZ1PJclZmppFSufjEajl5Tjwtm218vJHHQ0inkUyCg027bWztQShVZ0if3LubqIr7pE//5VzscCdJdYebXTF2rr35gk/MWD7u1o3ff1nIiETxW1bElXSiUSv2c0ijdUWou5j2s1bTiXPy6Bp6Yj5PABFtlVlVKxuCoKvLpk57ViuilyxicYulhl/woWW0dlvHjFaTNyP8yDGWfTrqtE7/zCS+kDBkTTOLQjXStoJpaSv/2HvWnCGviWbCeTd3AAA7AACbMDopqQ+DAAAaW0lEQVR4nO2dj38T9f3He59c4qVt7pKGpr1LLtKmTXKkoeVoSys9wg+bQog6mIBGvhs4/DK2MZmok84JCExRab/CUEat2FGViOXHWmudEzvAjl8ruqLGDexoBf3yFfVP+L4/d2mbtmmpHT9SHvdSyuXuk/B53vv9eb/fn89dekno+irLRlxfJamAKqAKqAKqgCqgCqgCqoAqoKKUmx3wprfgTQ9407toilYFHOuA+pse8PoSjgaQYeAPQry8geTtmwyQd+dbELJOwmiuUqvppgHEZmNMk5Aj3W0woXSDG/ZZ0x0Gky7N6uKZkVgysQGRKw3c0sBP8mDTTTXfCbvyHajE6k6yaswu1wgIExOQYcA8jDvNzBt0CGksaVaUaXFM1RjMCKVaYEfJbaUelOZQ2g6LmZiAyM0jk6HUko6SrQhllpgMaQbTVA9y5yNkSJ9iQAaX2S3DX1GJCuhJR+mlk25zW9LdGmsa4j187zGXB5mmahwmd5psPYfGNJwNExPQoWMMZrNBZzbwdzpcfJwWLqt5UrILPFlnsBgsw3xUwgHK1shPQ2YDmpqMx9yQ4t2TMhEaZzCnlSI3RNsxAgjmM4xjDB4YZhbLlVI6w3gMrnFTmHxD8hgBBPulpvEGk8Wg0WDPHB4Qjuqwjd1wMgxDNE4wQOQxuW/LT2KQx8zEG3pxT4nHYJ5qZUyOuMcTCpBBFgj9+YoxRlhwQjMPRBnImHfmx3tPQgEiq4FJzkST4ttiaPGafMZwJ4K3JjQgnH6TAeDGueIkcAanPPxDkgaZCQ/FqY7S2wxuXjfI8gkECPazmAyWdEd859TxOh0ymUwhKQ4+70L5pS6dwWqxJiogA9MEKKZNhrvQwMJEgtI07Qfz5s9b/MMfzZt29wLEOyW2/5udDBSoJoMVmXQDYlPiAJoM7nzDbUk6aZCFGNb6w7KFC8sWlf1g4aKF98y6N+Rk+wNK4LgmqGng/LgHvDlxAKFzGt5lQjwLFpT6Hblv8X+VLS+bsfBHP164ZPGMsmlLQ0zsUJPgFLCQ6KdazIZBtUHCAIJ0t5UAzv0/WdavjzyyzFs+bdYD08ru/u/5d0/70axZDyy/fVKsG0uS7/77If6YboMJx0AlCiD4GM9LFkZifrrip4zA9Lkg7/5Z2azbF959ryfk9rhLf754cdk9sxa7Uc/yjMAKzl+s/IUkIzOD/PuqAurjbo4MkHH6nIAnsb98cJVOcvYAgrf+auHiRT9b4IoOO5fDMH/5PUvyo8dZlnGGsh/CgHgk+pzX2IJ6EiT/+H6AEFl+vfonTjnN3f/wI9lCnwua580Ak5misRV+aMrmld09CSnnABoKQv6jP1kGppOcv17tu7aAAGaD/74/IPTu16sfk91Mcq6p/I25L1bcu6Ts5zMeFwRlD4sk0+Lli+dPcyEhakLTb+c+4cQveOdjk5ddY0DbxOK8nAmjAUROn+MRH8OwUnGVaJ/rAAtJLA6oC5bMuHv+Wog1kowDUXbt/BlLZrkQy2AHReZ1YoWtgMX8peuXXVsXJW32PCebOyoLgqz+dU+GnM4NNpHjgk/xkL1ZRkCmeYsWls0yAToW5HzTvIVLliyV4LXAOhnHXE4vkhsgU+h+O90xuNweMeDATg98rdfD2JuQF2I2Zuj1owQsp6bPRsIaSqQpyr/JzLJOgWV1Dyz8HeQ+vCyBPdgy7Z4lS3635AETBBVBMD013UtTIrUGJk2bRPHaAgJWgTBnvG3wsREDElzF064qThQpguLmOlinhITQA8tnzLhn0bR7F1iQbvMzs2YsWrRoxqwF0N7HWJ4VaY4T7QTx5HMzOao881oCknptjuDL0upJ+eAoAN3lFE2LfoIQods0TZc/ZZJYn8+0FkOVLV9++32Pz1i0pAx4f+6RoPr05c/l4ER4RZGgt5TTYMnMaOQZGrCnX3qCopRORv8i5PCPdxOUfhCg3EhPpmycqCUp+RA51GkbFpDwclRVdVaKnwJCirM/6xagBNPde/e8+b/7n0WLpk1bsmjRLMP8+zz8MiR5ni/nKApa+tc8IVYRtJcAQHbgh8YCwoY2Q5ZWqyUJpadERoyg26TNpjTS2mwUJpOVkQHkepuNJPSc3pZhs9n0Q0AODwg9LoBkvwH+dWzEyq3bTJC+IUTed3vZ7b/f/s6Pf3X7UhcuvxF/aPsWrxdOOEW+wEjZuRk0d0VAiiQ25ii6NUXUKmktKyenILozJxcnALuyXZBTlCVG8aicvBywnGJZyjY+rxhG4vcHtJQTNFHNOJ3CiylV0HU6+FBdYc02q9nl8uzYvmrnH1bW7ax57CX88q71q1avrAuCwYmMNS8ipyBUw3tF9xUAbfZsQQ7GcMqyUzJwxtbmot6yCQkTod9ZyInTEU44vhQlqU+YIzj73FV7K4tu1Q7lpsNbkCayQvDhZqlgjY30105++eVdhQ8WFhZO3jn5oddfmbzN8U7NTng9eXLN9rtK6+qCBGnPZc2QEoUUckSAcxgpD+STJJ7JycAOmsMyvjxFxcV2IB4vCLhNNjRxSkVyRBF51tfzKRSlLWKFW22kXh830AwDaAZAfVHerRs2PMz7sv9Yvnv14fr6+lcPv/bxx8+8ft/ePUcnz0a8xzp727ZDVg/MkdYWhoNrCpDwXG5uQXYuSdEVAyeDAwD1pD3bidaItH5CVrHEC3CM0uYgVNQ70KCRdjxiizMIvWhPyUMSPxEP1QlQRFLRAKXXa3PBgqOJohhQzKqCEVxuCkl8++pn6v+054036vfWH057pPHN1zEgK0mbQ1KoocHCMA1rV0ZMIZ6fnmHLEDdCBC6/ggUxIOubiCOHViwW2DywoAxo08uSG8mAWjCbzWb3gakyMCB/1QApmtLTXJXLJxzaWbLnjX319Xv27Nt3eH/TgUf3vwKAOlfD7QdRaOnSl0LPnJbWP7h1GZNdwYFEeGfFgisDMgyMM4hMWhhqLGxqcxiE/RCiFSDoZUChWEtCI9qW44TNOIDoPwIkIBm+yG679Hr9vj/V1+/b9wb4af3axqaj2ILSwbealy5gDy7d3FzWICxbv3MV+2QVTpo0tqBlBIDOiTjYQ+DgWSZFqwBCRoh6aQ8gsFKEtgiFinGwvVqA0+HtNGGjOfLtDVtbjgIduCdmrK9/pfGRvfIYPHjw0ObNQqh58/LmlxoY5p2dG/5Mk3IkILhy88gsiFMYTfqc7HgtJQPaom2wnyqAYE4CIqzgLMD32Qwcg6ME9EzHof7dd6G29Nsqf/oXMN2effWKXj7S9OrLACgsfW/zodPNC97a3Lx0aQNrWX3KXsXBEMTVHTlSQNlWdgA8YaOiFlTs1wcIPkvoM4pZNvfqAorvr1jR2hr4a7mda9x5eN8bL3+6/80jRzoO7z165HMMKCxY2ry0eddbzR/vaj7oQr5VETtX8e77AXjP+1X0SADZqAW1KQhl26NBBlctWLhaU6IoxBgtmcs6l00kryYg+UG4ra2tMXzpnEj4L72z5+jT7Y2Nxxobd3dMadz98tHC2Q0NzUvfat5x6L1dy5e/19Dw0t9mElXHWusijW3HIx+MyIKsYkGbOEdABTAxAEBpTkFxMfxfnAulmmxB04kTJ7I+KhYQq9wJdrUAZ5KXj1URUHL5j/m9dGfhq6807W5UdLmxEQeZ0GsNBze/dvrkrs1Lm997fNmucAUlHguWw4Ah2jqpijjXSwe5KD8RsgqRUuwUsjGqNgemlUg5MXlVOG+ABWFuDX8EZ3GWjSaIwXlwdICsZ7oYaf0ryRGE1wtl4/SVrx9tAvt98AEAHvvgyN5XJ89u2Nzw+OZDv/jodAMYcsH9q47ZCPsEDio2219bI/aKkYxBoQCMlccKTt947KsAyOYVKNUoTvjYglBLsYIwR8gr18o3Y14lQOSaTh9pXXHORlMkRxNU+aVn6l850HjqwDFswgNT6sGCprdemt3ccPLkS+8d2vHWQXNNEPKXzUZ7xb+3Ptg+IkA2JIDFWFYqPiGndwgyLIxBZfqAF82wi+aBhxaFIIRWk7Z4gKNM9K6ZxLpVEGPe1cOswsuJ4V1vvNJ0eXd60+enGhub6jGg4Go+vX3z6RcP7Tjd/F6D9ZIfkgPHEe8GVqyoOeMdURR1QslZXFB0QomZCiCeGuAJu56QgwxEUS2pnZgnCHgO0Quo7wUECw55n+0wLuqaSdmffWfnitYP34XzQ3PhlqOPfrm76ejew7uPNK2FVFE4G3kONu869MuPth167eBmITNcwdEcJX7Q+o/WVd9WU9NHlia02Fg9FlAqGWjTM8FVSjV5IsVCVaCkFCcriDZlomuD9CixowXkvFt+WLPiH60Q88GCLY82HmmHgnvvm40d9TIgTOQbmp/Z3nl66eYGn2QNV0Bl93cw387fP2unqZmuEQGSclk2PCCe3JPFrDzjkGcT7ERlvRePUhi2RaMCnEt5adJ+pmPyCkiFpD+8a/3+R0twmm86snafAigg3iQUnxRCJiQg96UK27sfYvOd8dMk7R0RoHNiv+EzNCBlS0Gszy5bLQ/f/K1XCIkMCFFD3+s+DKCpMoOkOJryb9q+Evx0et3H+VPWH95Xv+9oR9Pe+n17FQuyjLRjhwA+wyDz6iD2zppPKjmYLBG2mYOvvcTLg3EACX3vBD3GgmSe5NyIvRWSJU7+UcIToTn8hO97bQLEuKwFRXYI+ZQ3eOafrSsirc+U3vLpfjDgp0379+7bsw+PQYZhWOHkDnkdXzIVRlpXFL5ZacfVNpVV8KKHHXbhtwcwdllTqUUVl4UZrJzo8RjEb9LmOqU8G9jKliXxfEqGHJe0VHEIPHcUgKzLygpFGdiGNBf85A8PrXz500fWdqyv3/vpo4+sh6r76IOzIT1JklT7Fb6awfrQqkjdZ7+x49kSZcsSBIuHHXZVrWcMDgIE45C4VIOpKBkDSE6E6WcWrkuhKnWyRSJOJify2Dm4RPj+gILrLsTOScGAMCXltvyz8PCnTU2apqdKSpqanv5XSem/Js/GF5CyT15u3HF/ts/HoNk1z4o4TwDg23kMa4lzA+mVLcgKvuwenehzUXwwoxgJ8nTCZteF5rC+4oIC8FpB2mgbzboouCiSQuzGKpKG4E/4Vz60f/16+f/9HR37/7J+//6V2746e7a9R2d3vHSozo/ngVAZrHEKbMjtGhRjRgQol2r4B8QOMhbQNh6hOVCRg2HtxRKLLx/A4MjOgs+k4sEND4h0+P4Y1gkDkSK9dLA1fC5G78Of8Dm8Kyz/OHfuw3Mfhi814hkymbUhG3+A1TX4Q/st/JJibnHOhH5ruuSJgj7l2Em9bWJx8caoB5JcTnHORDkB2jJO5BSD2+QVjM8Y0nojAmScAlRIWtvxcDkMDHk1ltJnVPgrystFu0iJeO0Epn9wikVabNtaRRDirSFBNl28u9X7A5L6jIz+l/fwyOsVntnj1z0LgngdCswdzfBaG/538azqPwIELwiFcnNfqGvDzseBt3r9WzZ1hj85+9mmdU94YYRSYDVvMGinCK//b20bNxQj5xw5emZeGRB6S3EDLi0oV2318j4K+ztQKi7IUcrFXPyDxovceMmfwE4zekA8EninIDxWWCHDeTn/s8cDkY5Ad1dge7h2nR8v11P+z2s7/1xJUN7Or+RrvIIcPTPj3CI1YOkeX3vofwWaInrLGnyBDOf8fiNMHz0D8rIUNB7OeiMDxGKEmk49BEea9vqPBLr/ifG6AjUXAu3tfs6rtze1V576er2f454o1PXlvisBXlHRlSd97Ot4bUYJyMfchGcCD8Xpmwv+u7P7fO2FQEeg63zL+cjxmvZKL+U/Odfr/e7sl9Vcec2OvtTnuEqAZOzrgW2Gu/j5PQB/eakC43n9nYHzF7oBrivQcb69tavuQqCp0kv8+yOSqqzdHuSqasFHxwxgad/2YxERL3YGm7raI4GWQMt57KIYsiXQtX8LRDOCq3z066CX7twq9N4bk+iAuhjArZ04VD7RHfjifKTr0oXu7q66SEtde/f5SKA9cvbME3a7/8utJ4M0d6CG783ujjg3CSfKnU4DLbj1cwLi54VwbTjwRV3L2UDL2Uh3SyRy/sL5zsDxcMvFi893fBX5qpzmZhaaegFLxw6gVLOb8hLVLQaD4WLgfG16V+3Wr8OR44Fwd/fx82DR2u7artrwmyLlnVnYV5/lJzggU9J7D4FU0wlZVvzybFeHwRCBJNHx2cWu2kA40t1+vhso27+ojdR8HeQIrm3ymAFkmaecPonBd+IxaFWtSNH6k1sDkUiH4TMYiYFAi+FI+/GmS5GuQPvK9kBt7UfTOY4gjheanPh2Z5bnfVOcwujvsrj2gE5Bs8YkOKGKERhh286gl6LXdTa1fxHpiDx/saslHGiprKr2z/0kciHcfbL983V+L0Vw/sKtIUFiJWYOm73mDJKudJfFjQQU0Djx7TkCFCYA6Zl83E7Rwa8idRBBA1ufv2gwTA3PFf3VXHVwS1uwEqZJnJcixcYHn1MmO6yUIhqRb9g1mRsNyLhnEtUpWW8XpaTkMtt2tlGE+FlXS0ekq72jpe5i5GLXuac6v9wSFEkR5sOUfC1x5s5V0pNFRbkbUzZk0dMdTOha3075nwAygu6MFzfBV8vX3NqyE7Lcqdq6SHtH19nas59017YH/re1szawqfLAl3aowznKFgwXPpdSpSXx7SsE941n8H1ACQXIojQ/hedINPa/6bWXTtnt674+UtcSqW3prot8VtdV23X2LGTESK3fyxE28YlAXVDk5Dk9zDHsz0MQTmQXBUDzNxMoSrkgTUEZ2nrBX36qdtOXtS0fdrW0b23f+n/ttUc6d3f+u1IkSMJ/oLUuiCeMtAzIzbUm+Fd7IMYLt1RylMJHc7S/beWl422Von/dJ1+f6Tx14HIkMDfoB1WToui/XLfyiB+3Vghp+52DzZdYgJAIWd0dEDs5eeEQILlgY/hSIND9YTgS3n1gur8q6G/b/cH74fCH4UuXwrv9YtR+0JKasCn+lykTCRDL8q2diI5D3HOwVGVb4+XLx4+H61ov+9taV0aO7758ubGtMlje1w4iqvebtWPi+4MScnwr3ywa7TllU5YKCL0YrKztDn+3xW6n5JWTnsGn3EHi/SaNdQ4OoYkHyPLIcYefono7z3m9Xryw6/WWi/7GIF0uyru8epL2cr181WfS8JrlGABErIDMhrn2XkAqeqmL9vs5ohpc0U/bsGvicYr/wmtc3qChFLLo4OsSiQiIpSu5Y65/grJkjcMpRYkVW7ZwnFgA4P6ZIr7tR1njwtnBHjwzyeMc8vuwiQgowNz+tm/nBkW7CCovn/vdc0+yDwNvNSC/4Hzx6XUzK0QRO6rdH3z2jlQzXlMfS4AM40Ssy52fZtTcklaSadaxAuN8QTEo8R0jIB4O3vL8mTPf3plc6tZBdpH4MQUINhT6f7+OQc9V6cmJxSkcST0sCQPDpRDnS6GJDThYL75N0Po/2gmi+rk53+uNYwWQ2ULqK4rKSZvfN7Lv1481QM+fiwryhLwNG962hm5KQCfEVhwsGSTETXdjHnDUUgFVQBVQBbzWgNeV74YAUtdcsYC6qEy66yE+S0teXyVprq9uud5KUqVKlSpVqlSpUqVKlSpVqlSpUqVKlSpVqlSpUqVKlSpVqlSNERlBqcPuToXNvq04jeXdsdtDfOYNUaojM9NaokkesNso75Z7nZpvsTiMuEHqOGumNTPZOLBtmjUzM1MBSi3B21j5yRrjwE+9ATIqv8fXPaDTRvn3VTHJuIfpJoR08hkwKncHT+lvnNRx8l6zRn4Rc9OQK19znSiGkdGsPHkiuV+nU0sRfuYkGof3alw9gKmM/LwmT/9+a5Tfq2PCe1PT+h4Ny2DoG25DjVl5KKa1nwk1HvxUTTQIED9sc9DZSJMfwQmAyQog8rjdbgt+hCMTNeuNFAbkPYjRaZL6TrbcTey7gwERfo6PNXZwYSd3mVAMoDXdaNRoNG5szNIbPQ4BEDH4MW75MVbBfZafXhcH0A3G1cXaRQPWc8QA9npDOn5SjutGOykGRJN4PLD6eqLBzwIdCrC0/9kwOsAF0nVxAJPkRx4ODLnXWwogfrxG3z65z8bMIVw0nUcxQytZ44J9k+ID4lA85fryDJIMmD4lplu4z4CQPiQgPhu9FpRzxDhNXECjeXBOue5SADUmRgnzWHKfS4a0oKZEDjM97wdcU3p8QNmCaQkBaMyMGVhKn4cENOIH9Zp6RmwqOKzDGBcwGUYmwyREkElPMjJ9AwtvO1KHBjRa+84GPjO8MSkuoBGHI09CBJl0JVtE+wVkfGrS0IBJyb0ZPFmjw5vJsYBR/03VpOHno+cnCGBqfs+ZT8bj0aIZDlB+j1KHlyhhpL8FMyHRG6e4GQYlRiWDAbENlIGl9DlpOMBUXBhkGqESx+82pSf1B2SUihXXMaYbbb9ewCR5YJWk9vV5OBcFx2Rw0E1NxhUBFGP9LdgrtyY5EYptGbBnYEX7PCygMscqSUrGZ4XHRhoECOW3y5oM9W3iAOKkBWEG+szzGGs4QBkDzka6Th6uAwGt4AhQbd9498TqBUzFv6TMYUzno30eDlDexRvlPKA0GRxFk5OSbrj1sHoBYboOAyu9tAdrWMBU/Ng3R7onOs2NnwcTQzGAeKaX5urp87CA8uKFK1m2ufwpYwEwdQouPKIh5kqAGnw2zAz4qeyIYwIwSWOSF1IYpc/DA0JBLic7i5LIxwagUXl6uaWvYhsaEJ8N1NNgrABGl/ym9JbRwwEqZ8MVrcQUwKTYKJooigXEC1C9iyhXAMSFQc9wTWhAPOtmFMBkueLu6bPR0WPNWMDeJeJUXBjwPaU0dlhXX7GdSIBJxinjeufcxuSStN7OGdNKoHCDjWTjuHFRKmPMcc2Ukt7rDzFNUmOaJIaMqX2LCqkx28lJqT21SO9eOB6zemqMfdHTOPYjVKlSpUqVKlWqVKlSpUqVKlWqVKlSpUqVKlWqVKlSpUqVKlWqVKlSpUrVTaH/B6ZN7g4fVI5iAAAAAElFTkSuQmCC" alt="" />
  </div>
</div>
    </div>
</section>

<section className="text-gray-700 body-font border-t border-gray-200">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full mb-20">
      <h2 className="text-xs text-red-500 tracking-widest font-medium title-font mb-1">ALIADOS</h2>
      <h1 className="sm:text-3xl text-2xl font-medium title-font text-gray-900">Contamos con el respaldo </h1>
    </div>
    <div className="flex flex-wrap -m-4">
      <div className="p-4 md:w-1/3">
        <div className="flex rounded-lg h-full bg-gray-100 p-8 flex-col">
          <div className="flex items-center mb-3">
            <div className="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-500 text-white flex-shrink-0">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg>
            </div>
            <h2 className="text-gray-900 text-lg title-font font-medium">Convoca</h2>
          </div>
          <div className="flex-grow">
            <p className="leading-relaxed text-base">Convoca es una organización de periodismo de investigación fundada en 2014 por reporteros, analistas de datos y programadores en Perú, bajo el liderazgo de la periodista Milagros Salazar Herrera. </p>
            <a className="mt-3 text-red-500 inline-flex items-center">Lee  mas
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div className="p-4 md:w-1/3">
        <div className="flex rounded-lg h-full bg-gray-100 p-8 flex-col">
          <div className="flex items-center mb-3">
            <div className="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-500 text-white flex-shrink-0">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg>
            </div>
            <h2 className="text-gray-900 text-lg title-font font-medium">Sociedad Nacional de Radio y Televisión: SNRTV</h2>
          </div>
          <div className="flex-grow">
            <p className="leading-relaxed text-base">La Sociedad Nacional de Radio y Televisión es una organización gremial que agrupa a los principales canales de televisión y emisoras de radio del Perú. Está asociada a la Confederación Nacional de Instituciones Empresariales Privadas.</p>
            <a className="mt-3 text-red-500 inline-flex items-center">Lee  mas
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div className="p-4 md:w-1/3">
        <div className="flex rounded-lg h-full bg-gray-100 p-8 flex-col">
          <div className="flex items-center mb-3">
            <div className="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-500 text-white flex-shrink-0">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-5 h-5" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg>
            </div>
            <h2 className="text-gray-900 text-lg title-font font-medium">ministerio de la mujer y poblaciones vulnerables</h2>
          </div>
          <div className="flex-grow">
            <p className="leading-relaxed text-base">es el órgano del Estado Peruano dedicado a la mujer y al derecho en la sociedad de los peruanos. Su sede principal está en Lima, Perú.</p>
            <a className="mt-3 text-red-500 inline-flex items-center">Lee  mas
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section className="text-gray-700 body-font border-t border-gray-200">
  <div className="container px-5 py-24 mx-auto flex flex-wrap">
    <div className="lg:w-1/2 w-full mb-10 lg:mb-0 rounded-lg overflow-hidden">
      <img alt="feature" className="object-cover object-center h-full w-full" src= {mujer2} />
    </div>
    <div className="flex flex-col flex-wrap lg:py-6 -mb-10 lg:w-1/2 lg:pl-12 lg:text-left text-center">
      <div className="flex flex-col mb-10 lg:items-start items-center">
        <div className="w-12 h-12 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 mb-5">
         <span className='text-lg title-font font-medium '>1</span>
        </div>
        <div className="flex-grow">
          <h2 className="text-gray-900 text-lg title-font font-medium mb-3">Establece un titulo</h2>
          <p className="leading-relaxed text-base">Lorem ipsum dolor, sit amet consectetur adipisicing elit.  </p>
          <a className="mt-3 text-red-500 inline-flex items-center">Lee  mas
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
              <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
          </a>
        </div>
      </div>
      <div className="flex flex-col mb-10 lg:items-start items-center">
        <div className="w-12 h-12 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 mb-5">
        <span className='text-lg title-font font-medium '>2</span>
        </div>
        <div className="flex-grow">
          <h2 className="text-gray-900 text-lg title-font font-medium mb-3">Describe el suceso</h2>
          <p className="leading-relaxed text-base">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
          <a className="mt-3 text-red-500 inline-flex items-center">Lee  mas
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
              <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
          </a>
        </div>
      </div>
      <div className="flex flex-col mb-10 lg:items-start items-center">
        <div className="w-12 h-12 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 mb-5">
        <span className='text-lg title-font font-medium '>3</span>
        </div>
        <div className="flex-grow">
          <h2 className="text-gray-900 text-lg title-font font-medium mb-3">Identifica a los medios</h2>
          <p className="leading-relaxed text-base">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis est veniam possimus. Laborum vero quis alias, modi quos, q </p>
          <a className="mt-3 text-red-500 inline-flex items-center">Lee  mas
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
              <path d="M5 12h14M12 5l7 7-7 7"></path>
            </svg>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section className="text-gray-700 body-font border-t border-gray-200">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-wrap w-full mb-20 flex-col items-center text-center">
      <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">Sumate a la causa</h1>
      <p className="lg:w-1/2 w-full leading-relaxed text-base">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reprehenderit corporis eligendi fuga qui reiciendis, quasi impedit dicta earum maiores amet, debitis corrupti quibusdam, iste natus! Excepturi architecto libero beatae incidunt.</p>
    </div>
    <div className="flex flex-wrap -m-4">
   
      <div className="xl:w-1/3 md:w-1/2 p-4">
        <div className="border border-gray-300 p-6 rounded-lg">
          <div className="w-10 h-10 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 mb-4">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-6 h-6" viewBox="0 0 24 24">
              <circle cx="6" cy="6" r="3"></circle>
              <circle cx="6" cy="18" r="3"></circle>
              <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
            </svg>
          </div>
          <h2 className="text-lg text-gray-900 font-medium title-font mb-2">Corta el ciclo</h2>
          <p className="leading-relaxed text-base">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
        </div>
      </div>
      <div className="xl:w-1/3 md:w-1/2 p-4">
        <div className="border border-gray-300 p-6 rounded-lg">
          <div className="w-10 h-10 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 mb-4">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-6 h-6" viewBox="0 0 24 24">
              <path d="M12 22s8-4 8-10V5l-8-3-8 3v7c0 6 8 10 8 10z"></path>
            </svg>
          </div>
          <h2 className="text-lg text-gray-900 font-medium title-font mb-2">Protegete</h2>
          <p className="leading-relaxed text-base">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
        </div>
      </div>
      <div className="xl:w-1/3 md:w-1/2 p-4">
        <div className="border border-gray-300 p-6 rounded-lg">
          <div className="w-10 h-10 inline-flex items-center justify-center rounded-full bg-red-100 text-red-500 mb-4">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-6 h-6" viewBox="0 0 24 24">
              <path d="M4 15s1-1 4-1 5 2 8 2 4-1 4-1V3s-1 1-4 1-5-2-8-2-4 1-4 1zM4 22v-7"></path>
            </svg>
          </div>
          <h2 className="text-lg text-gray-900 font-medium title-font mb-2">Alerta a a comunidad</h2>
          <p className="leading-relaxed text-base">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
        </div>
      </div>
      <div className="xl:w-1/3 md:w-1/2 p-4">
         
      </div>
   
    </div>
    <button className="flex mx-auto mt-16 text-white bg-red-500 border-0 py-2 px-8 focus:outline-none hover:bg-red-600 rounded text-lg">Denuncia</button>
  </div>
</section>
<section className="text-gray-700 body-font border-t border-gray-200">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col text-center w-full mb-20">
      <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Nuestro Euipo</h1>
      <p className="lg:w-2/3 mx-auto leading-relaxed text-base">Conocenos</p>
    </div>
    <div className="flex flex-wrap -m-2">
      <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
          <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src="https://dummyimage.com/80x80/edf2f7/a5afbd"/>
          <div className="flex-grow">
            <h2 className="text-gray-900 title-font font-medium">Mey Chang  </h2>
            <p className="text-gray-500">Fundador</p>
          </div>
        </div>
      </div>
      <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
          <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src="https://dummyimage.com/84x84/edf2f7/a5afbd"/>
          <div className="flex-grow">
            <h2 className="text-gray-900 title-font font-medium">Yenifer  Galarza</h2>
            <p className="text-gray-500">Fundador</p>
          </div>
        </div>
      </div>
      <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
          <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src="https://dummyimage.com/88x88/edf2f7/a5afbd"/>
          <div className="flex-grow">
            <h2 className="text-gray-900 title-font font-medium">Ronaldo Arias</h2>
            <p className="text-gray-500">Fundador</p>
          </div>
        </div>
      </div>
        

      <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
          <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src="https://dummyimage.com/88x88/edf2f7/a5afbd"/>
          <div className="flex-grow">
            <h2 className="text-gray-900 title-font font-medium">Ivan Cerna</h2>
            <p className="text-gray-500">Fundador</p>
          </div>
        </div>
      </div>

      <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
        <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
          <img alt="team" className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4" src="https://dummyimage.com/88x88/edf2f7/a5afbd"/>
          <div className="flex-grow">
            <h2 className="text-gray-900 title-font font-medium">Gustavo Mayahuasca</h2>
            <p className="text-gray-500">Fundador</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 
    </>)
}