import React,{useState} from 'react'
import { Link, useNavigate } from 'react-router-dom';
import {useAuth} from '../context/authContext';
import { Alert } from './Alert';
import '../App.css';
import logo from '../assets/logo.png'; 
export  function Register() {
    const [user,setUser]=useState({
        email:'',
        password:'',
    })

const {signup}= useAuth()
const navigate = useNavigate()
const [error,setError]=useState();

    const handleChange = ({target:{name,value}}) =>{
       
        setUser({...user,[name]: value})
    }

    const handleSubmit=async (e)  =>{
        e.preventDefault();
        setError( '')
            try  {
                await signup(user.email,user.password) ;
            //  navigate('/');
            }
            catch(error){
                console.log(error);
        setError(error.message)
        }  
}
  return (
    <div className='w-full h-screen flex bg-img--container'>
   <div className='w-full max-w-xs m-auto '>
{error && <Alert message={error}/>}
<form onSubmit={handleSubmit} className="bg-white shadow-md rouded px-8 pt-6 pb-8 mb-4">
<div className="flex w-100 justify-center ">
    <img className='w-40' src={logo} alt="Logo" />
    </div>
<div className="mb-4">
  <label htmlFor="email" className='block text-gray-700 text-sm font-bold mb-2'>Email</label>
  <input type="email" name="email" placeholder="" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
  focus:shadow-outline' onChange={handleChange}/>
</div>

<div className="mb-4" >
  <label htmlFor="password" className='block text-gray-700 text-sm font-bold mb-2'>Password</label>
  <input type="password" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
  focus:shadow-outline text-sm' name="password" id="" onChange={handleChange} />
</div>


<div className="mb-4" >
  <label htmlFor="name" className='block text-gray-700 text-sm font-bold mb-2'>Nombres</label>
  <input type="text" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
  focus:shadow-outline text-sm' name="name" id="" onChange={handleChange} />
</div>


<div className="mb-4" >
  <label htmlFor="lastname" className='block text-gray-700 text-sm font-bold mb-2'>Apellidos</label>
  <input type="text" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
  focus:shadow-outline text-sm' name="lastname" id="" onChange={handleChange} />
</div>



<div className="mb-4" >
  <label htmlFor="dni" className='block text-gray-700 text-sm font-bold mb-2'>Documento </label>
  <input type="number" className='shadow apparence-none border rounded w-full py-2 px-2 text-gray-700 leadign-tight  focus:outline-none 
  focus:shadow-outline text-sm' name="dni" id="" onChange={handleChange} />
</div>


<button className='bg-red-500 hover:bg-red-700  text-white font-bold py-2 px-4 reounded focus:outline-none focus:shadow-outline'> Registrate</button>

  </form>
  <p className='my-4 text-sm flex justify-between px-4'> ya tienes una cuenta? <Link to="/login">Login</Link> </p>
   </div></div>
  )
}

