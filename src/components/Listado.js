import { useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/logo.png'; 
import img2 from '../assets/pexels-cottonbro-10464476.jpg'; 
import img4 from '../assets/pexels-cottonbro-5053850.jpg'; 

import '../App.css';

export function Listado( )
{

   const  handleSubmit =()=>{

   }
   const [user,setUser]=useState({
      email:'',
      password:'',
  })

 

  const handleChange = ({target:{name,value}}) =>{
     
      setUser({...user,[name]: value})
  }

    return  (<>
  <header className="border-b">
  <div className="container">
    <div className="flex items-center justify-between">
      <div className="logo">
        <img className="h-20 p-2" src={logo}  alt="logo"/>
      </div>
      <nav className="navbar flex items-center gap-8">
        <p   className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900"> <Link to="/"> Home</Link> </p>
        <p className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900">  <Link to="/recursos"> Recursos</Link> </p>
        <p  className="nav-link font-semibold uppercase text-gray-500 hover:text-gray-900">  <Link to="/feed">  </Link> </p>
 
      </nav>
    </div>
  </div>
</header>

<article className="max-w-5xl mx-auto py-24 px-5 min-h-screen grid place-content-center">
  <div className="text-center">
    <span className="bg-red-500 text-white uppercase py-1 px-3.5 rounded-full text-sm">categoria 1</span>
    <h1 className="text-5xl text-gray-900 font-semibold mt-2 mb-5">Denuncias  </h1>
    <p className="text-red-500 text-lg max-w-xl mx-auto">Explore y apoye otros motivos</p>
  </div> 
    
  <div className="flex gap-5 my-10 flex-wrap sm:flex-nowrap">
   
    <div className="card card--two rounded-xl overflow-hidden shadow-lg w-full md:w-2/6">
      <img src={img2}  alt="Coffee" className="w-full object-cover h-36"/>
      <div className="p-7">
        <h2 className="font-semibold text-3xl text-gray-900 mb-3">Contenido violento en horaio familiar</h2>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam erat volutpat. Integer imperdiet lectus quis justo. Ut enim ad minim veniam, quis nostrud exercitation.</p>
        <div className="flex-1 inline-flex items-center">
        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 48 48">
    <path d="M24 24c4.42 0 8-3.59 8-8 0-4.42-3.58-8-8-8s-8 3.58-8 8c0 4.41 3.58 8 8 8zm0 4c-5.33 0-16 2.67-16 8v4h32v-4c0-5.33-10.67-8-16-8z"/>
    <path d="M0 0h48v48h-48z" fill="none"/>
</svg>
                            <p><span className="text-gray-900 font-bold">700</span> Colaboradores</p>
                        </div>
                        
                        <div className='flex'>
                           
                        <img className="bg-cover bg-center   h-10 rounded-full mr-3" src="https://i0.wp.com/www.wric.com/wp-content/uploads/sites/74/2022/01/FKdoDzEWYAcHzNq.jpg?w=2000&ssl=1"/>
                             <div>
                                <p className="font-bold text-gray-900">juana romero</p>
                                <p className="text-sm text-gray-700">(555) 555-4321</p>
                                </div></div>
      </div>
    </div>

    <div className="card card--two rounded-xl overflow-hidden shadow-lg w-full md:w-2/6">
      <img src="https://trome.pe/resizer/N-cOW_R5sSdOmEdc8EyE3j_8ias=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/ZVQUBHNNKBGGDGBUB25D7QAKF4.png" alt="Coffee" className="w-full object-cover h-36"/>
      <div className="p-7">
        <h2 className="font-semibold text-3xl text-gray-900 mb-3">Magaly Discurso de odio programa de 28 de marzo</h2>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam erat volutpat. Integer imperdiet lectus quis justo. Ut enim ad minim veniam, quis nostrud exercitation.</p>
        <div className="flex-1 inline-flex items-center">
        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 48 48">
    <path d="M24 24c4.42 0 8-3.59 8-8 0-4.42-3.58-8-8-8s-8 3.58-8 8c0 4.41 3.58 8 8 8zm0 4c-5.33 0-16 2.67-16 8v4h32v-4c0-5.33-10.67-8-16-8z"/>
    <path d="M0 0h48v48h-48z" fill="none"/>
</svg>
                            <p><span className="text-gray-900 font-bold">1000</span> Colaboradores</p>
                        </div>
                        <div className='flex'>
                        <img className="bg-cover bg-center h-10 rounded-full mr-3" src="https://this-person-does-not-exist.com/img/avatar-9967f089c4074e4a02394bc813b6fabf.jpg"/>
                            
                               <div>
                               <p className="font-bold text-gray-900">anonimo  </p>
                               
                               </div>
                            </div>
      </div>
    </div>

    <div className="card card--two rounded-xl overflow-hidden shadow-lg w-full md:w-2/6">
      <img src="https://scontent.flim16-3.fna.fbcdn.net/v/t1.18169-9/1958391_852732018098336_3825144298127145637_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=9267fe&_nc_ohc=HueFvy6TvU0AX-H0ZZz&_nc_ht=scontent.flim16-3.fna&oh=00_AT96xPUqR08d86gQmnmU5Dpml2325tPhRf9uEOfXtlIaZQ&oe=626466AF" alt="Coffee" className="w-full object-cover h-36"/>
      <div className="p-7">
        <h2 className="font-semibold text-3xl text-gray-900 mb-3">invasion de la privacidad en propiedad privada</h2>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam erat volutpat. Integer imperdiet lectus quis justo. Ut enim ad minim veniam, quis nostrud exercitation.</p>
        <div className="flex-1 inline-flex items-center">
        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 48 48">
    <path d="M24 24c4.42 0 8-3.59 8-8 0-4.42-3.58-8-8-8s-8 3.58-8 8c0 4.41 3.58 8 8 8zm0 4c-5.33 0-16 2.67-16 8v4h32v-4c0-5.33-10.67-8-16-8z"/>
    <path d="M0 0h48v48h-48z" fill="none"/>
</svg>
                            <p><span className="text-gray-900 font-bold">400</span> Colaboradores</p>
                        </div>
                        <div className='flex'>
                        <img className="bg-cover bg-center  h-10 rounded-full mr-3" src="https://caricom.org/wp-content/uploads/Floyd-Morris-Remake-1024x879-1-500x429.jpg"/>
                            
                        <div>
                               <p className="font-bold text-gray-900">juan galvez</p>
                                <p className="text-sm text-gray-700">(01) 555-4321</p>
                               </div>
                            </div>
      </div>
    </div>


  </div>
  <div className="text-center">
    <a href="#" className="bg-red-600 inline-block text-white py-3 px-5 rounded hover:bg-red-700">Mas</a>
  </div>
</article>
    </>)
}